FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/larva_postgres.jar /larva_postgres/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/larva_postgres/app.jar"]
