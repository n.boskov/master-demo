-- :name create-festival! :i!
-- :doc creates a new festival record
INSERT INTO Festivals
(ime, lokacija)
VALUES (:ime, :lokacija)

-- :name update-festival! :! :n
-- :doc update an existing festival record
UPDATE Festivals
SET ime = :ime, lokacija = :lokacija
WHERE id = :id

-- :name get-festival :? :1
-- :doc retrieve a festival given the id.
SELECT * FROM Festivals
WHERE id = :id

-- :name delete-festival! :! :n
-- :doc delete a festival given the id
DELETE FROM Festivals
WHERE id = :id
