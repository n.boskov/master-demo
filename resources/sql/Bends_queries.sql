-- :name create-bend! :i!
-- :doc creates a new bend record
INSERT INTO Bends
(ime, zanr, velicina)
VALUES (:ime, :zanr, :velicina)

-- :name update-bend! :! :n
-- :doc update an existing bend record
UPDATE Bends
SET ime = :ime, zanr = :zanr, velicina = :velicina
WHERE id = :id

-- :name get-bend :? :1
-- :doc retrieve a bend given the id.
SELECT * FROM Bends
WHERE id = :id

-- :name delete-bend! :! :n
-- :doc delete a bend given the id
DELETE FROM Bends
WHERE id = :id
