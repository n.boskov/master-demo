-- :name get-muzicar-priznanja :? :*
-- :doc returns priznanja associated with muzicar
SELECT * FROM Muzicars__priznanja__smpl_coll
WHERE muzicars_id = :muzicar

-- :name assoc-muzicar-priznanja! :!
-- :doc associates muzicar with corresponding priznanja
INSERT INTO Muzicars__priznanja__smpl_coll (muzicars_id, priznanja)
VALUES 
/*~
(let [single (:muzicar params)]
    (clojure.string/join
        ", " (for [m (:priznanja params)]"(" single ", " m ")")))
~*/

-- :name dissoc-muzicar-priznanja! :!
-- :doc dissociates priznanja from muzicar
DELETE FROM Muzicars__priznanja__smpl_coll
WHERE muzicars_id = :muzicar AND priznanja IN :tuple:priznanja

-- :name dissoc-all-muzicar-priznanja! :!
-- :doc dissociates all priznanja from muzicar
DELETE FROM Muzicars__priznanja__smpl_coll
WHERE muzicars_id = :muzicar

-- :name get-muzicar-bend :? :1
-- :doc returns bend associated with muzicar
SELECT * FROM Bends
WHERE id = (SELECT bend FROM Muzicars WHERE id = :muzicar)

-- :name get-bend-clanovi :? :*
-- :doc returns clanovi associated with bend
SELECT * FROM Muzicars
WHERE bend = :bend

-- :name assoc-muzicar-bend! :!
-- :doc associates muzicar with corresponding bend
UPDATE Muzicars SET bend = :bend
WHERE id = :muzicar

-- :name assoc-bend-clanovi! :!
-- :doc associates bend with corresponding clanovi
UPDATE Muzicars SET bend = :bend
WHERE id IN :tuple:clanovi

-- :name dissoc-muzicar-bend! :!
-- :doc dissociates muzicar from corresponding bend
UPDATE Muzicars
SET bend = NULL
WHERE id = :muzicar

-- :name dissoc-bend-clanovi! :!
-- :doc dissociates bend from corresponding clanovi
UPDATE Muzicars
SET bend = NULL
WHERE id IN :tuple:clanovi

-- :name dissoc-all-bend-clanovi! :!
-- :doc dissociates all bend from corresponding clanovi
UPDATE Muzicars
SET bend = NULL
WHERE bend = :bend


