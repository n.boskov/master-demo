-- :name create-muzicar! :i!
-- :doc creates a new muzicar record
INSERT INTO Muzicars
(ime, prezime, nadimak, bend)
VALUES (:ime, :prezime, :nadimak, :bend)

-- :name update-muzicar! :! :n
-- :doc update an existing muzicar record
UPDATE Muzicars
SET ime = :ime, prezime = :prezime, nadimak = :nadimak, bend = :bend
WHERE id = :id

-- :name get-muzicar :? :1
-- :doc retrieve a muzicar given the id.
SELECT * FROM Muzicars
WHERE id = :id

-- :name delete-muzicar! :! :n
-- :doc delete a muzicar given the id
DELETE FROM Muzicars
WHERE id = :id
