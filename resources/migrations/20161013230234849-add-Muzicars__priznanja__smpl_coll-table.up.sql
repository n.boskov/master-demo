CREATE TABLE Muzicars__priznanja__smpl_coll
(id SERIAL PRIMARY KEY,
 muzicars_id INTEGER REFERENCES Muzicars(id),
 priznanja VARCHAR(30));
