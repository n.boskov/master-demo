CREATE TABLE Muzicars
(id SERIAL PRIMARY KEY,
 ime VARCHAR(30),
 prezime VARCHAR(30),
 nadimak VARCHAR(30),
 bend INTEGER);
