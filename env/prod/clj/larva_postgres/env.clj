(ns larva-postgres.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[larva_postgres started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[larva_postgres has shut down successfully]=-"))
   :middleware identity})
