(ns user
  (:require [mount.core :as mount]
            larva-postgres.core))

(defn start []
  (mount/start-without #'larva-postgres.core/repl-server))

(defn stop []
  (mount/stop-except #'larva-postgres.core/repl-server))

(defn restart []
  (stop)
  (start))


