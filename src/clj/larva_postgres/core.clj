(ns larva-postgres.core
  (:gen-class)
  (:require [clojure.string :as string]
            [clojure.tools
             [cli :refer [parse-opts]]
             [logging :as log]]
            [larva-postgres
             [config :refer [env]]
             [handler :as handler]]
            [larva-postgres.db.core :as db]
            [larva.frameworks.luminus.build :as larva]
            [luminus
             [http-server :as http]
             [repl-server :as repl]]
            [luminus-migrations.core :as migrations]
            [mount.core :as mount]))

;; [larva.frameworks.luminus.build :as larva]
;; [larva-postgres.db.core :as db]
;; [clojure.string :as string]

(def cli-options
  [["-p" "--port PORT" "Port number"
    :parse-fn #(Integer/parseInt %)]])

(mount/defstate ^{:on-reload :noop}
  http-server
  :start
  (http/start
   (-> env
       (assoc :handler (handler/app))
       (update :port #(or (-> env :options :port) %))))
  :stop
  (http/stop http-server))

(mount/defstate ^{:on-reload :noop}
  repl-server
  :start
  (when-let [nrepl-port (env :nrepl-port)]
    (repl/start {:port nrepl-port}))
  :stop
  (when repl-server
    (repl/stop repl-server)))


(defn stop-app []
  (doseq [component (:stopped (mount/stop))]
    (log/info component "stopped"))
  (shutdown-agents))

(defn start-app [args]
  (doseq [component (-> args
                        (parse-opts cli-options)
                        mount/start-with-args
                        :started)]
    (log/info component "started"))
  (.addShutdownHook (Runtime/getRuntime) (Thread. stop-app)))

(defn -main [& args]
  (cond
    (some #{"migrate" "rollback"} args)
    (do
      (mount/start #'larva-postgres.config/env)
      (migrations/migrate args (select-keys env [:database-url]))
      (System/exit 0))
    :else
    (start-app args)))

;; play
(def test-model
  {:about
   {:name    "Pilot model"
    :author  "Pera Peric"
    :comment "Model muzickog festivala."}
   :meta
   {:api-only false
    :db       {:type :postgres :sql :hugsql}}
   :entities
   [{:signature  "Muzicar"
     :properties [{:name "ime" :type :str}
                  {:name "prezime" :type :str}
                  {:name "nadimak" :type :str}
                  {:name "priznanja" :type {:coll :str}}
                  {:name "bend" :type {:one :reference
                                       :to  ["Bend"]}}]}
    {:signature  "Bend"
     :properties [{:name "ime" :type :str}
                  {:name "zanr" :type :str}
                  {:name "velicina" :type :str}
                  {:name "clanovi" :type {:coll :reference
                                          :to   ["Muzicar" "bend"]}}]}
    {:signature  "Festival"
     :properties [{:name "ime" :type :str}
                  {:name "lokacija" :type :geo}]}]})

;; (larva/make :model test-model :force true)

;; (let [{pazzy :id}  (db/create-muzicar! {:ime     "Vinnie"       :prezime "Paz"
;;                                         :nadimak "Pazzy answer" :bend    nil})
;;       {apathy :id} (db/create-muzicar! {:ime     "Chad"             :prezime "Bromley"
;;                                         :nadimak "The Alien Tongue" :bend    nil})
;;       {aotp :id}   (db/create-bend! {:ime  "Army of the Pharaohs"
;;                                      :zanr "Rap" :velicina 15})]

;;   (db/assoc-bend-clanovi! {:bend aotp :clanovi [pazzy apathy]})

;;   (let [pharaohs (map (fn [muzicar]
;;                         (update muzicar :nadimak
;;                                 #(string/upper-case (str % ", The Pharaoh!"))))
;;                       (db/get-bend-clanovi {:bend aotp}))]
;;     (str "You're listening " (string/join " and " (map :nadimak pharaohs))
;;          " representing the " (:ime (db/get-bend {:id (-> pharaohs
;;                                                           first
;;                                                           :bend)})))))
